import { Component, OnInit } from '@angular/core';
import { booklist } from '../models/book-list.model';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.sass']
})
export class BooksComponent implements OnInit {
  books: booklist[];
  constructor() {
    this.books = [];
  }

  ngOnInit(): void {
  }

  savebook(namebook: string, authorbook: string, numpages: number):boolean {
    this.books.push(new booklist(namebook, authorbook, numpages));
    console.log(this.books);
    return false;
  }

}
