export class booklist {
    namebook: string;
    authorbook: string;
    numpages: number;

    constructor(namebook: string, authorbook: string, numpages: number){
        this.namebook = namebook;
        this.authorbook = authorbook;
        this.numpages = numpages;
    }
}